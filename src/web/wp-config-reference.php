<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/* Composer Autoloader */
require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

/* Load .env file */
$dotenv = new Dotenv\Dotenv( __DIR__ . DIRECTORY_SEPARATOR . '..' );
$dotenv->load();

// ** MySQL settings - You can get this info from your web host ** //
define( 'DB_NAME', getenv( 'WP_DB_NAME' ) );

/** MySQL database username */
define( 'DB_USER', getenv( 'WP_DB_USER' ) );

/** MySQL database password */
define( 'DB_PASSWORD', getenv( 'WP_DB_PASSWORD' ) );

/** MySQL hostname */
define( 'DB_HOST', getenv( 'WP_DB_HOST' ) ?: 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', getenv( 'WP_DB_CHARSET' ) ?: 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE',  getenv( 'WP_DB_COLLATE' ) ?: '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         getenv( 'WP_AUTH_KEY' ) );
define( 'SECURE_AUTH_KEY',  getenv( 'WP_SECURE_AUTH_KEY' ) );
define( 'LOGGED_IN_KEY',    getenv( 'WP_LOGGED_IN_KEY' ) );
define( 'NONCE_KEY',        getenv( 'WP_NONCE_KEY' ) );
define( 'AUTH_SALT',        getenv( 'WP_AUTH_SALT' ) );
define( 'SECURE_AUTH_SALT', getenv( 'WP_SECURE_AUTH_SALT' ) );
define( 'LOGGED_IN_SALT',   getenv( 'WP_LOGGED_IN_SALT' ) );
define( 'NONCE_SALT',       getenv( 'WP_NONCE_SALT' ) );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = getenv( 'WP_DB_PREFIX' ) ?: 'wp_' ;

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define( 'WP_DEBUG', getenv( 'WP_DEBUG' ) ?: false );

/* Use HTTPS for login / admin */
define( 'FORCE_SSL_LOGIN', getenv( 'WP_FORCE_SSL_LOGIN' ) ?: false );
define( 'FORCE_SSL_ADMIN', getenv( 'WP_FORCE_SSL_ADMIN' ) ?: false );

/* Fix reverse proxy https://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && stripos( $_SERVER['HTTP_X_FORWARDED_PROTO'], 'https' ) !== false ) {
	$_SERVER['HTTPS'] = 'on';
}

/* Custom site URLs */
$my_scheme = 'http';
if ( ! empty( $_SERVER['HTTPS'] ) ) {
	$my_scheme = 'https';
}
$my_base_url = getenv( 'WP_BASE_URL' ) ?: $my_scheme . '://' . filter_input( INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_URL );
define( 'WP_SITEURL', $my_base_url . '/wp' );
define( 'WP_HOME', $my_base_url );

/* Custom content directory */
define( 'WP_CONTENT_URL', $my_base_url . '/wp-content' );
define( 'WP_CONTENT_DIR', __DIR__ . DIRECTORY_SEPARATOR . 'wp-content' );

/* Expose caching functionality */
define( 'WP_CACHE', getenv( 'WP_CACHE' ) ?: false );

/* Clean up database */
define( 'WP_POST_REVISIONS',  getenv( 'WP_POST_REVISIONS' ) ?: 5 );
define( 'MEDIA_TRASH',  getenv( 'WP_MEDIA_TRASH' ) ?: false );
define( 'EMPTY_TRASH_DAYS',  getenv( 'WP_EMPTY_TRASH_DAYS' ) ?: 14 );

/* Increase memory for WooCommerce */
define( 'WP_MEMORY_LIMIT', getenv( 'WP_MEMORY_LIMIT' ) ?: '64M' );

/**
 * Admin settings that will work with multidomains out-of-the-box
 * http://kaspars.net/blog/wordpress/wordpress-multisite-without-domain-mapping
 * http://tommcfarlin.com/resolving-the-wordpress-multisite-redirect-loop/
 */
/*
define( 'ADMIN_COOKIE_PATH', '/' );
define( 'COOKIE_DOMAIN', '' );
define( 'COOKIEPATH', '' );
define( 'SITECOOKIEPATH', '' );
*/

/* Multisite */
define( 'WP_ALLOW_MULTISITE', false );

/* Updates https://codex.wordpress.org/Configuring_Automatic_Background_Updates */
define( 'WP_AUTO_UPDATE_CORE', getenv( 'WP_AUTO_UPDATE_CORE' ) ?: false );
define( 'AUTOMATIC_UPDATER_DISABLED', getenv( 'WP_AUTOMATIC_UPDATER_DISABLED' ) ?: false );

/* Security */
define( 'DISALLOW_FILE_MODS', getenv( 'WP_DISALLOW_FILE_MODS' ) ?: false );
define( 'DISALLOW_FILE_EDIT', getenv( 'WP_DISALLOW_FILE_EDIT' ) ?: false );
define( 'WP_HTTP_BLOCK_EXTERNAL', getenv( 'WP_HTTP_BLOCK_EXTERNAL' ) ?: false );
define( 'WP_ACCESSIBLE_HOSTS', getenv( 'WP_ACCESSIBLE_HOSTS' ) ?: filter_input( INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_URL ) . ',*.wordpress.org,wordpress.org' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
