<?php
/**
 * Must Use hooks
 *
 * @package WordPress
 */

/* Disable XML-RPC */
add_filter( 'xmlrpc_enabled', '__return_false' );

/* Hide pingback URL */
add_filter(
	'wp_headers',
	function( $headers ) {
		unset( $headers['X-Pingback'] );
		return $headers;
	}
);

/* Disable plugin updates */
/* add_filter( 'auto_update_plugin', '__return_false' ); */

/* Disable theme updates */
/* add_filter( 'auto_update_theme', '__return_false' ); */

/* Clean up HTML <head> */
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'start_post_rel_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'adjacent_posts_rel_link' );

/* Clean up WordPress backend */
if ( is_admin() ) {
    add_action(
        'admin_menu',
        function() {
			/* Hide admin menu item */
            //remove_menu_page( 'index.php' ); // Dashboard.
            //remove_menu_page( 'edit.php' ); // Posts.
            //remove_menu_page( 'upload.php' ); // Media.
            //remove_menu_page( 'edit.php?post_type=page' ); // Pages.
            //remove_menu_page( 'edit-comments.php' ); // Comments.
            //remove_menu_page( 'themes.php' ); // Appearance.
            //remove_menu_page( 'plugins.php' ); // Plugins.
            //remove_menu_page( 'users.php' ); // Users.
            //remove_menu_page( 'tools.php' ); // Tools.
            //remove_menu_page( 'options-general.php' ); // Settings.
            //remove_menu_page( 'edit.php?post_type=custom_post_type' ); // Custom post type.

			/* Remove news from dashboard */            
            remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );
        }
    );

	/* Remove welcome panel from dashboard */
    remove_action( 'welcome_panel', 'wp_welcome_panel' );
}
